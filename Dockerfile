FROM openjdk:8-jre-slim

RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

ADD ./build/libs/beverage-vendor-0.1.0.jar app.jar

CMD java -jar ${ADDITIONAL_OPTS} app.jar

EXPOSE 9003
